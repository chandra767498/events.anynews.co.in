import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AppService } from 'src/app/app.service';
import { CookieService } from 'ngx-cookie-service';
@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.scss'],
})
export class EventsComponent implements OnInit {
  dataObj: any = {};
  isevent: boolean;
  leaderboard: any;
  isquizpage: boolean;
  constructor(
    private appService: AppService,
    private cookieService: CookieService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    const Params = this.route.snapshot.params;
    this.dataObj['draft_id'] = Params.draft_id;
    this.submitForm(this.dataObj);
    this.isevent = false;
    this.isquizpage = false;
  }
  submitForm(data) {
    try {
      this.appService.postMethod('events', data).subscribe(
        (resp: any) => {
          if (resp.success) {
            if (resp.event_status) {
              this.isevent = true;
              this.leaderboard = resp.leaderboard;
              if (resp.event_schedule_status) {
                this.isquizpage = true;
              }
              this.isquizpage = false;
            }
            this.isevent = false;

          } else {
            // swal.fire(resp.msg, 'Something went wrong!', 'error');
          }
        },
        (error) => {}
      );
    } catch (e) {}
  }
}
